# Simply Settings - Unreal Engine Plugin to create a Settings Widget
#### An easy way to add a settings menu to your Unreal Engine Project
*Create your Unreal Engine Settings menu easy and fast using this Plugin. It's easy to integrate with existing projects and the design can be adapted to your project. It has a graphics, input bindings, audio and gameplay menu. Furthermore, it also allows to setup, store and read custom settings easily with simple blueprint nodes.*
#### [Download Plugin UE 5.3](Built%20Versions/SimplySettings-5.3-v2.0.zip)

##### [Download Plugin UE 5.1 - Legacy](Built%20Versions/SimplySettings-5.1-v1.1.zip)
##### [Download Plugin UE 5.0 - Legacy](Built%20Versions/SimplySettings-5.0-v1.1.zip)
##### [Download Plugin UE 4.27 - Legacy](Built%20Versions/SimplySettings-4.27-v1.0.zip)
##### [Download Plugin UE 4.26 - Legacy](Built%20Versions/SimplySettings-4.26-v1.0.zip)
Please reference the legacy branch readme for details about the usage of the legacy version

![This is an image](https://media.giphy.com/media/YxaoyvnEm6FaKte5fW/giphy.gif)

***
Features:
- Plug and Play: Use the PreSet to setup Settings in a few minutes
- Set and save Graphic settings inkl. auto detect
- Set and save Input settings (enhanced input)
- Assignable Keys
- Automatically generate Action Bindings (multiple row)
- Blacklist Keys
- Set and save Volume settings
- Adjust audio via Slider
- Set and save Custom Settings
- Save your own game settings

***
[![Discord](Resources/discord-logo-blue.png)](https://discord.gg/S7NuQa8Aeb)
***
## Documentation
### 1. Installation
You can get the plugin by clicking on the download links above. Make sure you use the correct version. It contains a private dependency plugin called SimplyWindow, which is used for presets. However, the download links include the complete source code.
After downloading, move both Folders to your "ProjectName/Plugins" directory.
For example, if your project is called Blue, move it to Blue/Plugins.
If no Plugins folder exists, simply create one.
You should now see the Plugin under "Edit->Plugins->UI" after starting the project.

At some point, the Settings must be loaded. Using the "Load Simply Settings" Blueprint Node, you can do this pretty much anywhere. The gameinstance is a good location. 

![This is an image](/Resources/Load-Settings.png)

With that completed, the installation is finished, and you can begin using it. 

### 2. Settings Menu Creation and Style Customization
There is a preset for the settings Widget that can be added to the viewport, its called UI_SettingsWidget.
You can pretty much add it anywhere to the viewport, where you can add standard widgets. 
It has to be created and the added to the viewport. 

![This is an image](/Resources/CreateSettingsWidget.png)

![This is an image](/Resources/CreateSettingsWidget.png)

This is just a standard widget. It contains the other settings widgets in a widget switcher. You can find those under the plugins content. 
To save some time, you can change the appearance of the buttons all at once by changing the master button called "SimplySetting_BaseButton". You find under "SimplySettings Content/SettingsButton/SimplySetting_BaseButton". Among all the standard button stuff, there is also a category called "active style", which is the style when the setting, that the button represents, is enabled. The default color is blue.

![This is an image](https://public-files.gumroad.com/uj0fuy72eyey4sgid61bwy5mtb74)

### 3. Graphics settings
The graphics settings work out of the box. There is a preset widget for the graphics settings which is located in the plugins content and already used in the UI_SettingsWidget. It includes the common graphics settings, if you need to add additional graphics settings, please reference the Custom Button section.
The functionality is mainly written in the "UGraphicsSettingsWidget" C++ class.

### 4. Input Settings
To configure the input settings, open the Simply Settings Configuration by navigating to Edit->Project Settings in the Engine. Scroll down to find the Simply Settings section. If there are keys that cannot be bound by the players, add them to the "Key Blacklist" section. In the example below, the H key is blacklisted.

**Under Contexts we have to list all the Enhanced Input Mapping Contexts that should be editable and/or saved by the plugin**.
The checkbox "Allow Multi Binding" allows you to select whether a key can be bound to multiple actions. When disallowed (default), unique context groups can be created. The Input Mapping Context groups created here contain only unique key Action bindings. For example, if a group with two Action Mapping Contexts called "Shooting Context" and "Moving Context" is created, each key can only be used exactly once in all the actions of both contexts. However, if separate groups are created for each context, it is possible to bind a key twice as long as it is in a different Input Action Context.

![This is an image](/Resources/Inputconfig.png)

In the Next step, we have to configure the actions, that should be mappable by the players. 
You can do that in the Action itself or overwrite it in the Input Mapping Context. 
It is important to give each of them a unique name. To do that, set Player Mappable Key Settings under Player Mappable Key settings. After this selection, you can define the name and category. While name is absolutely required for the correct binding, category can be empty if you don't want to categorize your bindings.

![This is an image](/Resources/InputActionMappingExample.png)

After this setup, open the UI_InputSettings in the Simply Settings Plugins Content folder. 
In here you can create the bindings as needed. Just select the Settings key selector and define the context, Input Action and the name you've just set. After compiling, you will notice that the key will show up automatically. 

![This is an image](/Resources/KeySelector.png)

#### Automatic Binding Generation
You could continue with the method just explained by manually adding the key selectors and configuring the bindings. It gives you the maximal flexibility. But there is also an **alternative** way which will automatically generate the bindings here. Please note that it is experimental.

To enable the autogeneration we have to configure the "Input Auto Generate Input Scroll Bar," follow these steps:

1. Select the "Input Auto Generate Input Scroll Bar", it is already in the input widget.
2. Enable autogenerate by checking "Enable Auto Generate (Experimental)."
3. Select the Action Mapping Contexts. You can add up to 3 rows, and rows with the same display name will be grouped together based on the displayname.

To configure the sorting of categories (optional), follow these steps:

1. Click on the "Generate Categories" checkbox to add all your categories based on the Contexts you've added to the "Categories Sorting" array.
2. Define a sorting priority. For example, Actions have a priority of 1010, and Movement has a priority of 1020. This determines the order in which the categories are displayed.

You can remove anything below the scroll bar, as the plugin will automatically generate it.

![Autogenerate](/Resources/autogenerate.png)


To try it, you have to to open the game in the standalone mode. As you can see, all the configured bindings are generated automatically. 
> **Warning:** Do not test this in PIE, always use the standalone mode or the packaged version of your project

![Input Demo](/Resources/demoinput.png)

## 5. Audio Settings
To configure the audio settings, navigate to Edit->Project Settings in the Engine and scroll down to the Simply Settings section. Here, select the sound mix audio class combinations that should be saved and loaded by Simply Settings. You can also define default settings in this section.

![Audio Config](/Resources/Audioconfig.png)

You can create your own Audio Settings Widget based on the "SimplySettingsWidget" or customize the default one, you find it under "SimplySettings Content/UI_AudioSettings". Everything in here are default widget components, except the slider, which is a "Setting_Volume" component. You can find and customize it under "SimplySettings Content/SettingsButton/Setting_Volume" if needed. 

After adding such a slider, you can select the Sound Mix - Sound class combination, that you have added in the settings. Optionally, you can set a fade in time, this sets how fast the audio adapts to the new setting. 

![This is an image](/Resources/AudioWidget.jpg)

## 6. Custom Settings
The Plugin offers a very simple way to store your very own custom settings.

Similar to the Audio and Input settings, the custom settings can be configured in the project Settings. To access these settings, navigate to "Edit"->"Project Settings" once again.
In the last section of the settings page, you will find the Custom Settings, which consist of key-value pairs.
The left side represents the key name, and the right side represents the default value. 

![Custom Settings Config](/Resources/CustomSettingsConfig.png)

With that prepared we have to add it to our settings widget. You can add these new settings to your own Gameplay Settings Widget based on the "SimplySettingsWidget" or customize the default one found under "SimplySettings Content/UI_GameplaySettings. Just add a "Setting Custom" Button and configure on the right side the value it represents. Like in the following, the selected button represents a difficulty of two. While Difficulty is the key and the value is 2. Everything else is done automatically by the plugin.

![This is an image](/Resources/CustomSettingjpg.jpg)

The setting can now be read nearly anywhere. The value is always a string, but there are utility methods to get the value as a bool, float, or int. Just use the Get Custom Simply Setting node. 

![This is an image](/Resources/ReadCustomSetting.jpg)

## 7. Custom Button
There are defaults in the settings plugin for most common cases. 
But you can add any custom setting as needed. To do that, create a new Widget based on the "SimplySettings_Button". When opening the new blueprint, on the top left, you can overwrite the "onclick" and "isActive" event / function. 

The onclick should apply the custom setting on selection. The isactive returns whether the settings that the button represent is currently active. There are a lot of examples under "SimplySettings Content/SettingsButton/*" to take a reference. 

![This is an image](/Resources/CustomButton.jpg)

Now this new button can be added to any widget. Make sure to call the save / discard functions, if it's not saved directly. 
